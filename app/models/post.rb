class Post < ApplicationRecord
  mount_uploader :avatar, AvatarUploader
  
  validates :title, presence: true, length: {maximum: 140}
  validates :body, presence: true
end
